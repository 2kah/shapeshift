using UnityEngine;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public Shape CurrentShape = Shape.None;

	bool _gameRunning = false;
	float _startSpeed = 1.8f;
	float _speed = 1.8f;
	float _speedUpFactor = 0.2f;

	Dictionary<Shape, GameObject> _shapeMap = new Dictionary<Shape, GameObject>();

	public void Reset(Shape gateShape)
	{
		// Move to the start
		transform.position = new Vector3(0f, 0f, 0f);

		// Select a random shape
		NewRandomShape(gateShape);

		_speed = _startSpeed;
	}

	public void OnGameStart()
	{
		_gameRunning = true;
	}

	public void NewRandomShape(Shape gateShape)
	{
		var possibleShapes = new List<Shape>();
		if(gateShape != Shape.Cube && CurrentShape != Shape.Cube)
		{
			possibleShapes.Add(Shape.Cube);
		}
		if(gateShape != Shape.Sphere && CurrentShape != Shape.Sphere)
		{
			possibleShapes.Add(Shape.Sphere);
		}
		if(gateShape != Shape.Cone && CurrentShape != Shape.Cone)
		{
			possibleShapes.Add(Shape.Cone);
		}
		if(gateShape != Shape.Cylinder && CurrentShape != Shape.Cylinder)
		{
			possibleShapes.Add(Shape.Cylinder);
		}

		var rand = Random.Range(0, possibleShapes.Count);
		SwitchToShape(possibleShapes[rand]);
	}

	public void SpeedUp()
	{
		_speed += _speedUpFactor;
	}

	// Use this for initialization
	void Awake () {
		_shapeMap.Add(Shape.None, GameObject.FindGameObjectWithTag("None"));
		_shapeMap.Add(Shape.Cube, GameObject.FindGameObjectWithTag("Cube"));
		_shapeMap.Add(Shape.Sphere, GameObject.FindGameObjectWithTag("Sphere"));
		_shapeMap.Add(Shape.Cone, GameObject.FindGameObjectWithTag("Cone"));
		_shapeMap.Add(Shape.Cylinder, GameObject.FindGameObjectWithTag("Cylinder"));

		_shapeMap[Shape.Cube].SetActive(false);
		_shapeMap[Shape.Sphere].SetActive(false);
		_shapeMap[Shape.Cone].SetActive(false);
		_shapeMap[Shape.Cylinder].SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		if(_gameRunning)
		{
			transform.position = new Vector3(0f, 0f, transform.position.z + (_speed * Time.deltaTime));
		}
	}

	void SwitchToShape(Shape newShape)
	{
		// Deactivate previous shape
		_shapeMap[CurrentShape].SetActive(false);

		// Activate new shape
		CurrentShape = newShape;
		_shapeMap[CurrentShape].SetActive(true);
	}

}
