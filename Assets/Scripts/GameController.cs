using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public enum Shape
{
	None,
	Cube,
	Sphere,
	Cone,
	Cylinder
}

public class GameController : MonoBehaviour
{

	public Player player;
	public GameObject GatePrefab;
	public AudioSource MusicPlayer;
	public Text ScoreText;
	public AudioSource C4Audio;
	public Canvas GameOverCanvas;
	public Canvas StartCanvas;
	public GameObject E4Trigger;

	int _score = 0;
	int _gatePoolSize = 100;
	LinkedList<Gate> _gateList = new LinkedList<Gate>();
	bool _playerInGate = false;
	bool _gameOver = false;
	float _distanceBetweenGates = 10f;
	bool _gameStarted = false;

	public void PlayerEnteredGate(Shape gateShape)
	{
		_playerInGate = true;
		if(player.CurrentShape != gateShape)
		{
			Debug.LogFormat("Game over! Player is {0}, Gate is {1}", player.CurrentShape.ToString(), gateShape.ToString());
			//TODO
			Time.timeScale = 0f;
			_gameOver = true;
			GameOverCanvas.gameObject.SetActive(true);
			return;
		}

		player.SpeedUp();
		_score++;
		ScoreText.text = string.Format("Score: {0}", _score.ToString());

		C4Audio.Play();

	}

	public void PlayerExitedGate()
	{
		_playerInGate = false;

		// Move the first gate to the end
		var firstGate = _gateList.First.Value;
		firstGate.transform.position = new Vector3(0f, 0f, _gateList.Last.Value.transform.position.z + _distanceBetweenGates);
		_gateList.RemoveFirst();
		_gateList.AddLast(firstGate);

		player.NewRandomShape(_gateList.First.Next.Value.CurrentShape);
	}

	// Use this for initialization
	void Start () {
		if(!Application.isEditor)
		{
			Cursor.visible = false;
		}

		// Create pool of gates
		for(int i = 0; i < _gatePoolSize; i++)
		{
			_gateList.AddLast(((GameObject)Instantiate(GatePrefab, Vector3.zero, Quaternion.identity)).GetComponent<Gate>());
		}

		MoveGates();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}

		if(!_gameStarted && Input.GetKeyDown(KeyCode.Space))
		{
			StartCanvas.gameObject.SetActive(false);
			StartGame();
			_gameStarted = true;
		}

		if(!_playerInGate && !_gameOver)
		{
			if(Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1))
			{
				_gateList.First.Next.Value.ChangeShape(Shape.Cube);
			}
			if(Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2))
			{
				_gateList.First.Next.Value.ChangeShape(Shape.Sphere);
			}
			if(Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3))
			{
				_gateList.First.Next.Value.ChangeShape(Shape.Cone);
			}
			if(Input.GetKeyDown(KeyCode.Alpha4) || Input.GetKeyDown(KeyCode.Keypad4))
			{
				_gateList.First.Next.Value.ChangeShape(Shape.Cylinder);
			}
		}

		if(_gameOver)
		{
			if(Input.GetKeyDown(KeyCode.Space))
			{
				StartGame();
			}
		}
	}

	void MoveGates()
	{
		// Move gates
		_gateList.First.Value.transform.position = new Vector3(0f, 0f, -100f);

		int i = 1;
		for(LinkedListNode<Gate> node = _gateList.First.Next; node != null; node = node.Next)
		{
			node.Value.transform.position = new Vector3(0f, 0f, i * _distanceBetweenGates);
			var rand = Random.Range(1, 5);
			node.Value.ChangeShape((Shape)rand);

			i++;
		}
	}

	void StartGame()
	{
		if(_gameStarted)
		{
			MoveGates();
		}

		player.Reset(_gateList.First.Next.Value.CurrentShape);
		player.OnGameStart();

		//MusicPlayer.Stop();
		//MusicPlayer.Play();

		ScoreText.text = "Score: 0";

		E4Trigger.transform.position = new Vector3(0f, 1f, 15f);

		_score = 0;
		_playerInGate = false;
		_gameOver = false;
		GameOverCanvas.gameObject.SetActive(false);
		Time.timeScale = 1f;
	}
}
