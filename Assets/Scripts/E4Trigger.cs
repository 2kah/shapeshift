using UnityEngine;
using System.Collections;

public class E4Trigger : MonoBehaviour
{
	public AudioSource E4Audio;

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter()
	{
		E4Audio.Play();
		transform.position = new Vector3(0f, transform.position.y, transform.position.z + 10f);
	}
}
