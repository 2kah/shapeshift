using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gate : MonoBehaviour
{
	public Shape CurrentShape = Shape.None;

	Dictionary<Shape, GameObject> _gateMap = new Dictionary<Shape, GameObject>();
	GameController _gameController;

	public void ChangeShape(Shape newShape)
	{
		if(CurrentShape != Shape.None)
		{
			_gateMap[CurrentShape].SetActive(false);
		}

		CurrentShape = newShape;
		_gateMap[CurrentShape].SetActive(true);
	}

	// Use this for initialization
	void Awake ()
	{
		_gameController = GameObject.FindObjectOfType<GameController>();

		_gateMap.Add(Shape.Cube, GameObject.FindGameObjectWithTag("CubeGate"));
		_gateMap.Add(Shape.Sphere, GameObject.FindGameObjectWithTag("SphereGate"));
		_gateMap.Add(Shape.Cone, GameObject.FindGameObjectWithTag("ConeGate"));
		_gateMap.Add(Shape.Cylinder, GameObject.FindGameObjectWithTag("CylinderGate"));

		_gateMap[Shape.Cube].SetActive(false);
		_gateMap[Shape.Sphere].SetActive(false);
		_gateMap[Shape.Cone].SetActive(false);
		_gateMap[Shape.Cylinder].SetActive(false);
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	void OnTriggerEnter(Collider other)
	{
		_gameController.PlayerEnteredGate(CurrentShape);
	}

	void OnTriggerExit(Collider other)
	{
		_gameController.PlayerExitedGate();

		// Disable the collider for a while to prevent double activations
		GetComponent<Collider>().enabled = false;
		StartCoroutine(ReactivateTrigger());
	}

	IEnumerator ReactivateTrigger()
	{
		yield return new WaitForSeconds(3f);
		GetComponent<Collider>().enabled = true;
	}
}
